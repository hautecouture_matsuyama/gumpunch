﻿using UnityEngine;
using System.Collections;

public class EnableUI : MonoBehaviour {
    [SerializeField]
    GameObject SoundUI;
    [SerializeField]
    GameObject PanelUI;
    [SerializeField]
    GameObject GameOverPanelUI;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (!PanelUI.activeSelf)
        {
            SoundUI.SetActive(true);
        }
        else
        {
            SoundUI.SetActive(false);
        }
	}
}
