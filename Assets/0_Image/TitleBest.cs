﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TitleBest : MonoBehaviour {
    public Text BestScoreText;
	// Use this for initialization
	void Start () {
        BestScoreText.text = PlayerPrefs.GetInt("Score").ToString();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
