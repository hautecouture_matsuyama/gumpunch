﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActiveRankingCanvas : MonoBehaviour {

    private void OnEnable()
    {
        NendMenyManager.Instance.NendIconView(false);
        NendMenyManager.Instance.NendRecView(false);
    }

    private void OnDisable()
    {
        NendMenyManager.Instance.NendIconView(true);
        NendMenyManager.Instance.NendRecView(true);
    }
}
