﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using NendUnityPlugin.AD;

public class NendMenyManager : MonoBehaviour
{
    [SerializeField]
    GameObject BannerObs;
    [SerializeField]
    GameObject Rec;
    [SerializeField]
    GameObject Icon;
    [SerializeField]
    GameObject Icon2;

    public int SceneNo = 0;//0,title  1,Play   2,GameOver

    DFPBanner dfpBanner;

    //シングルトン
    private static NendMenyManager _instance = null;


    void Awake()
    {
        if (_instance == null)
        {

            _instance = this;
            DontDestroyOnLoad(gameObject);
            dfpBanner = new DFPBanner();
            dfpBanner.RequestBanner();
        }
        else
        {

            Destroy(gameObject);
        }
    }

    public static NendMenyManager Instance
    {
        get
        {
            return _instance;
        }
    }

    public void NendBannerView(bool viweFlag)
    {
        if (viweFlag)
        {
            BannerObs.GetComponent<NendAdBanner>().Show();
        }
        else
        {
            BannerObs.GetComponent<NendAdBanner>().Hide();
        }
    }

    public void NendRecView(bool viweFlag)
    {
        if (viweFlag)
        {
            Rec.GetComponent<NendAdBanner>().Show();
        }
        else
        {
            Rec.GetComponent<NendAdBanner>().Hide();
        }
    }

    public void NendIconView(bool viweFlag)
    {
#if UNITY_ANDROID

        if (viweFlag)
        {
            Icon.GetComponent<NendAdIcon>().Show();
        }
        else
        {
            Icon.GetComponent<NendAdIcon>().Hide();
        }
#endif
    }
    
    public void NendIcon2View(bool viweFlag)
    {
#if UNITY_ANDROID
        if (viweFlag)
        {
            Icon2.GetComponent<NendAdIcon>().Show();
        }
        else
        {
            Icon2.GetComponent<NendAdIcon>().Hide();
        }
#endif
    }

    public string GetRecName()
    {
        return Rec.GetComponent<NendAdBanner>().name;
    }

    public void ShowDfp()
    {
        dfpBanner.Show();
    }

    public void HideDfp()
    {
        dfpBanner.Hide();
    }
}
