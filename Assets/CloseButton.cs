﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CloseButton : MonoBehaviour {

    public GameObject RankingCanvas;

    public void CloseRanking()
    {
        NendMenyManager.Instance.NendRecView(true);
        NendMenyManager.Instance.NendIconView(true);
        RankingCanvas.active = false;
    }

    public void CloseChangeEdit()
    {
      if (NendMenyManager.Instance.SceneNo == 2)
        {
            NendMenyManager.Instance.NendIconView(true);
            NendMenyManager.Instance.NendRecView(true);
        }
        RankingCanvas.active = false;
    }

    public void CloseHelp()
    {
        RankingCanvas.active = false;
    }
}
