﻿using UnityEngine;
using System.Collections;
using GoogleMobileAds.Api;

public class DFPBanner {
    BannerView bannerView;
    AdRequest request;


public void RequestBanner()
{
    #if UNITY_ANDROID
    string adUnitId = "/17192736/CN_GameAPP_11_header";
    #elif UNITY_IPHONE
        string adUnitId = "/17192736/CN_GameAPP_11_header";
#else
        string adUnitId = "unexpected_platform";
#endif

    // Create a 320x50 banner at the top of the screen.
    bannerView = new BannerView(adUnitId, AdSize.Banner, AdPosition.Top);
    // Create an empty ad request.
    request = new AdRequest.Builder().Build();
    // Load the banner with the request.
    bannerView.LoadAd(request);
    
}

public void Show()
{
    bannerView.Show();
}

public void Hide()
{
    bannerView.Hide();
}
}
