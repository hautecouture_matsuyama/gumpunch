﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BannerCtrl : MonoBehaviour {

	static public BannerCtrl component;

	[SerializeField]
	private Vector2 hiddenBannerPos = 500 * Vector2.up;

	private RectTransform bannerTrfm{
		get{
			if(_bannerTrfm == null){
				_bannerTrfm = gameObject.GetComponent<RectTransform>();
			}
			return _bannerTrfm;
		}
	}
	private RectTransform _bannerTrfm;

	// Use this for initialization
	void Start () {
		DontDestroyOnLoad (transform.parent);

		component = this;

		Application.LoadLevel ("Scene");
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void SetBanner()
	{
        bannerTrfm.anchoredPosition = new Vector2(-53.84f, -10);
	}

	public void HideBanner()
	{
		bannerTrfm.anchoredPosition = hiddenBannerPos;
	}
}
