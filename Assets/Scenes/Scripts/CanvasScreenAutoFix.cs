﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class CanvasScreenAutoFix : MonoBehaviour {

    private void Awake()
    {
        if (1.0f * Screen.width / Screen.height < 9 / 16.0f)
        {
            this.gameObject.GetComponent<CanvasScaler>().matchWidthOrHeight = 0.7f;
        }
        else
        {
            this.gameObject.GetComponent<CanvasScaler>().matchWidthOrHeight = 1.0f;
        }
    }
}
