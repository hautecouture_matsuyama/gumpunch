﻿using UnityEngine;
using System.Collections;

public class SharePlugins : MonoBehaviour {
    public PlayerController share;

    public void OnClick()
    {
        if (gameObject.name == "Facebook")
        {
            share.ShareFacebook();
        }

        if (gameObject.name == "Line")
        {
            share.ShareLINE();
        }

        if (gameObject.name == "Twitter")
        {
            share.ShareTwitter();
        }
    }
}
