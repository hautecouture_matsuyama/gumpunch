﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class DebunekoTween : MonoBehaviour {

	[SerializeField]
	Vector3 rot1 = new Vector3(0, 0, -2f);
	[SerializeField]
	Vector3 rot2 = new Vector3(0, 0, 2f);
	[SerializeField]
	float timeInterval = 2f;
	[SerializeField]
	Ease easeType = Ease.InOutSine;

	//Vector3 initPos;

	// Use this for initialization
	void Start () {
		//initPos = transform.position;

		Invoke (((System.Action)ToLeft).Method.Name, 
		        timeInterval * Random.value);
	}

	void ToLeft()
	{
		transform.DOLocalRotate (rot1, timeInterval)
			.SetEase(easeType).OnComplete(ToRight);
	}

	void ToRight()
	{
		transform.DOLocalRotate (rot2, timeInterval)
			.SetEase(easeType).OnComplete(ToLeft);
	}
	
	// Update is called once per frame
	void Update () {
	

	}
}
