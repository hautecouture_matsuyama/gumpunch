﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class NekoMoveScript : MonoBehaviour {

	[SerializeField]
	private float jumpForce_y = 10;

	[SerializeField]
	private float jumForce_x = 2;

	[SerializeField]
	private float moveTime = 0.1f;

	[SerializeField]
	private float moveForce = 5f;

	[SerializeField]
	private Sprite[] punchSprites;
	//[SerializeField]
	//private int[] frameCount;

	[SerializeField]
	private float minActionInterval = 0.7f;
	[SerializeField]
	private float maxActionInterval = 2.5f;

	[SerializeField]
	private Camera mainCamera;

	[SerializeField]
	private Soundmanager smanager;

	private bool moving = false;

	private Rigidbody2D rgdby
	{
		get{
			if(_rgdbdy == null){
				_rgdbdy = gameObject.GetComponent<Rigidbody2D>();
			}

			return _rgdbdy;
		}
	}
	private Rigidbody2D _rgdbdy;

	private SpriteRenderer sprn
	{
		get{
			if(_sprn == null){
				_sprn = gameObject.GetComponent<SpriteRenderer>();
			}

			return _sprn;
		}
	}
	private SpriteRenderer _sprn;

	/*private Soundmanager smanager
	{
		get{
			if(_smanager == null){
				_smanager = gameObject.GetComponent<Soundmanager>();
			}

			return _smanager;
		}
	}
	private Soundmanager _smanager;*/

	private int actionCount = 4;

	private bool turnLeft = false;
	private bool turnRight = false;

	// Use this for initialization
	void Start () {
	
		//MoveHorizontal (1);

		//Jump (1);

		StartCoroutine (moveController());
	}
	
	// Update is called once per frame
	void Update () {

		if (Input.GetMouseButtonDown (0)) {
			//メインカメラ上のマウスカーソルのある位置からRayを飛ばす
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			
			//レイヤーマスク作成
			//int layerMask = LayerMaskNo.DEFAULT;
			
			//Rayの長さ
			float maxDistance = 10;
			
			RaycastHit2D hit = 
				Physics2D.Raycast((Vector2)ray.origin, 
				                  (Vector2)ray.direction, 
				                  maxDistance//, layerMask
				                  );
			
			//なにかと衝突した時だけそのオブジェクトの名前をログに出す
			if(hit.collider){
				Debug.Log(hit.collider.gameObject.name);

				if(hit.collider.tag == "movingCat"){
					smanager.PlayNormalSound();
				}
			}
		}

	}

	void OnTriggerEnter2D(Collider2D c)
	{
		if(c.tag == "turnleft"){
			turnLeft = true;
		}else if(c.tag == "turnright"){
			turnRight = true;
		}
	}

	IEnumerator moveController()
	{
		while(true){

			/*if(moving || rgdby.velocity.magnitude > 0){
				continue;
			}*/

			yield return new WaitForSeconds (
				Random.Range(minActionInterval, maxActionInterval));

			if(turnLeft){
				turnLeft = false;

				Turn(-1);

				Debug.Log("turnleft");
			}

			if(turnRight){
				turnRight = false;

				Turn (1);

				Debug.Log("turnright");
			}

			int actionIndex = Random.Range(0, actionCount + 1);
			
			int sign = Mathf.RoundToInt(Mathf.Sign(transform.localScale.x));
			
			if(!moving && rgdby.velocity.magnitude == 0){
				
				switch(actionIndex){
				case 0:
					Jump(sign);
					break;
				case 1:
					MoveHorizontal(sign);
					break;
				case 2:
					StartCoroutine(punchAction());
					break;
				case 3:
				case 4:
					Turn();
					break;
				default:
					MoveHorizontal(sign);
					break;
				}
				
			}

		}
		
	}
	
	//0
	void Jump(int dir)
	{
		float duration = 0.24f;
		
		transform.DORotate (new Vector3(0,0,dir*30), duration)
			.OnComplete(()=>{
				transform.DORotate (new Vector3(0,0,0), duration);
			});

		rgdby.AddForce 
			(new Vector2(dir * jumForce_x, jumpForce_y), 
			 ForceMode2D.Impulse);
	}

	//1
	void MoveHorizontal(int dir)
	{
		rgdby.AddForce 
			(new Vector2(dir * moveForce, 0), 
			 ForceMode2D.Impulse);
	}

	void stopMoving()
	{
		moving = false;
	}

	//2
	IEnumerator punchAction()
	{
		moving = true;

		int punchCount = Random.Range (1, 4);

		while(punchCount > 0){

			for(int i=0; i<punchSprites.Length; i++){
				sprn.sprite = punchSprites[i];
				
				yield return null;
			}

			punchCount--;
		}

		stopMoving ();
	}

	//3
	void Turn(int dir){
		transform.localScale = new Vector3 (
			dir * transform.localScale.x, 
			transform.localScale.y, 
			transform.localScale.z);
	}
	void Turn(){
		transform.localScale = new Vector3 (
			- transform.localScale.x, 
			transform.localScale.y, 
			transform.localScale.z);
	}
}
