﻿using UnityEngine;
using System.Collections;

public class NameCanvasTW : MonoBehaviour {

    [SerializeField]
    private GameObject JP;

    [SerializeField]
    private GameObject TW;

    void AWake()
    {
        if (Application.systemLanguage == SystemLanguage.Chinese)
        {
            TW.SetActive(true);
        }

        else
        {
            JP.SetActive(true);
        }
    }
}
